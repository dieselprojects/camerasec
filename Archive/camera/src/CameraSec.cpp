/*
 * CameraSec.cpp
 *
 *  Created on: Dec 12, 2014
 *      Author: idanhahn
 */

#include <cv.h>
#include <highgui.h>
#include "CameraSec.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <string>
#include <cctype>


using namespace cv;
using namespace std;

using boost::asio::local::stream_protocol;
using boost::asio::ip::tcp;

static string status;
const string videoStreamAddress = "rtsp://localhost:7001/live.sdp";

// ------------------------------- //
// -- Constructors Desctructors -- //
// ------------------------------- //

CameraSec::CameraSec() {
	status = "";
	cameraState = false;
}

CameraSec::~CameraSec() {
	// TODO Auto-generated destructor stub
}

void CameraSec::start(){

	cout << "starting \"start\""<< endl;
	int serviceState = 0;

	boost::asio::io_service my_io_service;
	::unlink("/tmp/cameraSocket");
	stream_protocol::endpoint ep("/tmp/cameraSocket");
	stream_protocol::acceptor acceptor(my_io_service,ep);
	stream_protocol::socket socket(my_io_service);
	acceptor.accept(socket);

	while (true){
		cout << "Waiting for socket input"<< endl;

		vector<char> recivedData(6);
		socket.read_some(boost::asio::buffer(recivedData));

		status = string(recivedData.data(),recivedData.size() - 2);

		if (status.size() > 4){
			cout << "User input error " << status << " Size: " << status.size() << endl;
			//break;
		}

		if (status.compare("STRT") == 0){
			cout << "User indicate STRT" << endl;
			while (cameraState == true){
				// wait for camera release
				cout << "Waiting for camera to be available" << endl;
				boost::this_thread::sleep(boost::posix_time::seconds(1));
			}

			boost::thread monitor_thread(&CameraSec::monitorThread,this);
			cout << "After thread creation" << endl;
			monitor_thread.detach();
		}
	}
}



// -------------------- //
// -- Find intruders -- //
// -------------------- //
vector< vector<Point> > findIntruders(Mat *image0g,VideoCapture *capture){

	Mat image1, image1g;
	Mat comparedImage;

	cout << "starting \"findIntruders\""<<endl;
	// take second image
	*capture >> image1;

	// change color to grey
	cvtColor(image1,image1g,CV_BGR2GRAY);

	// generate compared image
	absdiff(*image0g,image1g,comparedImage);

	// split image to channels (if needed for colored image, currently only one channel)
	vector<cv::Mat> channels;
	cv::split(comparedImage, channels); // greyScale will generate one channel

	// generate intruders list
	Mat d = cv::Mat::zeros(comparedImage.size(), CV_8UC1);
	for (unsigned int i = 0; i < channels.size(); i++)
	{
		Mat tmp;
		threshold(channels[i], tmp, 45, 255, CV_THRESH_BINARY);
		d |= tmp;
	}

	Mat kernel, e;
	getStructuringElement(cv::MORPH_RECT, cv::Size(10,10));
	morphologyEx(d, e, cv::MORPH_CLOSE, kernel, cv::Point(-1,-1));

	vector<std::vector<cv::Point> > contours;
	findContours(e.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	// List of "intruders" blobs
	vector< vector<Point> > intruders;
	for (unsigned int i = 0; i < contours.size(); i++)
	{
	    // Calculate area
	    double area = cv::contourArea(contours[i]);
	    // Only select large enough contours
	    if (area > 10000 && area < 90000)
	        intruders.push_back(contours[i]);
	}

	image1g.copyTo(*image0g);
	cout << "exiting \"findIntruders\", number of Intruders: " << intruders.size() << endl;
	return intruders;
}

// -------------------- //
// -- Monitor thread -- //
// -------------------- //

void CameraSec::monitorThread(){
	// start monitor thread, store previous image and take current image, compare and check for difference
	cout<<"starting \"monitorThread\""<<endl;
	Mat image0,image0g;
	bool firstNotify = true;

	VideoCapture capture(0);
	if (!capture.isOpened()){
		cerr<<"Error opening camera, Exiting"<<endl;
		exit(1);
	}
	cameraState = true;

	// get first image in gray scale
	capture >> image0;
	cvtColor(image0,image0g,CV_BGR2GRAY);

	while (status.compare("STRT") == 0){


		vector< vector<Point> > intruders = findIntruders(&image0g,&capture);

		if ( intruders.size() > 0 )
		{
			cout<<"Found Intruder"<<endl;


			// found intruder start notification sequence
			while (status.compare("STRT") == 0){

				cout << "Streaming" << endl;
				if ( firstNotify == true ){
					cout<<"first notification"<<endl;
					// first notification, send indication to user
					firstNotify = false;
				}


				//capture >> image1;

				//ret = avpicture_alloc(.data, c->pix_fmt, c->width, c->height);
			}

			// --------------------- //
			// --- END STREAMING --- //
			// --------------------- //
		}
		boost::this_thread::sleep(boost::posix_time::seconds(3));
	}
	cout << "Received global status " << status << endl;
	cameraState = false;
	capture.release();
}


