/*
 * CameraSec.cpp
 *
 *  Created on: Dec 12, 2014
 *      Author: idanhahn
 */

#include <cv.h>
#include <highgui.h>
#include "CameraSec.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <cctype>
extern "C" {
	#include <libavutil/avassert.h>
	#include <libavutil/channel_layout.h>
	#include <libavutil/opt.h>
	#include <libavutil/mathematics.h>
	#include <libavutil/timestamp.h>
	#include <libavformat/avformat.h>
	#include <libswscale/swscale.h>
	#include <libswresample/swresample.h>
}

#define STREAM_DURATION   10.0
#define STREAM_FRAME_RATE 25 /* 25 images/s */
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P /* default pix_fmt */

#define SCALE_FLAGS SWS_BICUBIC

#ifdef  __cplusplus

static const std::string av_make_error_string(int errnum)
{
    char errbuf[AV_ERROR_MAX_STRING_SIZE];
    av_strerror(errnum, errbuf, AV_ERROR_MAX_STRING_SIZE);
    return (std::string)errbuf;
}

#undef av_err2str
#define av_err2str(errnum) av_make_error_string(errnum).c_str()

#endif // __cplusplus




using namespace cv;
using namespace std;

using boost::asio::local::stream_protocol;
using boost::asio::ip::tcp;

static string status;

// Stream Variables:
// -----------------

// a wrapper around a single output AVStream
typedef struct OutputStream {
    AVStream *st;

    /* pts of the next frame that will be generated */
    int64_t next_pts;
    int samples_count;

    AVFrame *frame;
    AVFrame *tmp_frame;

    float t, tincr, tincr2;

    struct SwsContext *sws_ctx;
    struct SwrContext *swr_ctx;
} OutputStream;

const char *videoStreamAddress = "rtsp://localhost:7002/live.sdp";
static OutputStream video_st = { 0 }, audio_st = { 0 };
static AVOutputFormat *fmt;
static AVFormatContext *oc;
static AVCodec *audio_codec, *video_codec;
static int ret;
static int have_video = 0, have_audio = 0;
static int encode_video = 0, encode_audio = 0;
static AVDictionary *opt = NULL;
Mat camFrameg;

// ------------------------------- //
// -- Constructors Desctructors -- //
// ------------------------------- //

CameraSec::CameraSec() {
	status = "";
	cameraState = false;
}

CameraSec::~CameraSec() {
	// TODO Auto-generated destructor stub
}

void CameraSec::start(){

	cout << "starting \"start\""<< endl;

	boost::asio::io_service my_io_service;
	::unlink("/tmp/cameraSocket");
	stream_protocol::endpoint ep("/tmp/cameraSocket");
	stream_protocol::acceptor acceptor(my_io_service,ep);
	stream_protocol::socket socket(my_io_service);
	acceptor.accept(socket);

	while (true){
		cout << "Waiting for socket input"<< endl;

		vector<char> recivedData(6);
		socket.read_some(boost::asio::buffer(recivedData));

		status = string(recivedData.data(),recivedData.size() - 2);

		if (status.size() > 4){
			cout << "User input error " << status << " Size: " << status.size() << endl;
			//break;
		}


		// --------------------- //
		// -- Monitoring flow -- //
		// --------------------- //

		if (status.compare("STRT") == 0){
			cout << "User indicate STRT" << endl;
			while (cameraState == true){
				// wait for camera release
				cout << "Waiting for camera to be available" << endl;
				boost::this_thread::sleep(boost::posix_time::seconds(1));
			}

			boost::thread monitor_thread(&CameraSec::monitorThread,this);
			cout << "After thread creation" << endl;
			monitor_thread.detach();
		}


		// -------------------- //
		// -- Streaming flow -- //
		// -------------------- //

		if (status.compare("STRM") == 0){
			cout << "User Indicate STRM" << endl;


			while (cameraState == true){
				// wait for camera release
				cout << "Waiting for camera to be available" << endl;
				boost::this_thread::sleep(boost::posix_time::seconds(1));
			}

			boost::thread stream_thread(&CameraSec::streamThread,this);
			cout << "After stream thread creation" << endl;
			stream_thread.detach();

		}


	}
}



// -------------------- //
// -- Find intruders -- //
// -------------------- //
vector< vector<Point> > findIntruders(Mat *image0g,VideoCapture *capture){

	Mat image1, image1g;
	Mat comparedImage;

	cout << "starting \"findIntruders\""<<endl;
	// take second image
	*capture >> image1;

	// change color to grey
	cvtColor(image1,image1g,CV_BGR2GRAY);

	// generate compared image
	absdiff(*image0g,image1g,comparedImage);

	// split image to channels (if needed for colored image, currently only one channel)
	vector<cv::Mat> channels;
	cv::split(comparedImage, channels); // greyScale will generate one channel

	// generate intruders list
	Mat d = cv::Mat::zeros(comparedImage.size(), CV_8UC1);
	for (unsigned int i = 0; i < channels.size(); i++)
	{
		Mat tmp;
		threshold(channels[i], tmp, 45, 255, CV_THRESH_BINARY);
		d |= tmp;
	}

	Mat kernel, e;
	getStructuringElement(cv::MORPH_RECT, cv::Size(10,10));
	morphologyEx(d, e, cv::MORPH_CLOSE, kernel, cv::Point(-1,-1));

	vector<std::vector<cv::Point> > contours;
	findContours(e.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	// List of "intruders" blobs
	vector< vector<Point> > intruders;
	for (unsigned int i = 0; i < contours.size(); i++)
	{
	    // Calculate area
	    double area = cv::contourArea(contours[i]);
	    // Only select large enough contours
	    if (area > 10000 && area < 90000)
	        intruders.push_back(contours[i]);
	}

	image1g.copyTo(*image0g);
	cout << "exiting \"findIntruders\", number of Intruders: " << intruders.size() << endl;
	return intruders;
}

// -------------------- //
// -- Monitor thread -- //
// -------------------- //

void CameraSec::monitorThread(){
	// start monitor thread, store previous image and take current image, compare and check for difference
	cout<<"starting \"monitorThread\""<<endl;
	Mat image0,image0g;
	bool firstNotify = true;

	VideoCapture capture(0);
	if (!capture.isOpened()){
		cerr<<"Error opening camera, Exiting"<<endl;
		exit(1);
	}
	cameraState = true;

	// get first image in gray scale
	capture >> image0;
	cvtColor(image0,image0g,CV_BGR2GRAY);

	while (status.compare("STRT") == 0){


		vector< vector<Point> > intruders = findIntruders(&image0g,&capture);

		if ( intruders.size() > 0 )
		{
			cout<<"Found Intruder"<<endl;


			// found intruder start notification sequence
			while (status.compare("STRT") == 0){

				cout << "Streaming" << endl;
				if ( firstNotify == true ){
					cout<<"first notification"<<endl;
					// first notification, send indication to user
					firstNotify = false;
				}


				//capture >> image1;

				//ret = avpicture_alloc(.data, c->pix_fmt, c->width, c->height);
			}

			// --------------------- //
			// --- END STREAMING --- //
			// --------------------- //
		}
		boost::this_thread::sleep(boost::posix_time::seconds(3));
	}
	cout << "Received global status " << status << endl;
	cameraState = false;
	capture.release();
	cout << "Exiting \"monitorThread\"" << endl;
}



// ---------------------- //
// -- Stream functions -- //
// ---------------------- //


/* Add an output stream. */
static void add_stream(OutputStream *ost, AVFormatContext *oc,
                       AVCodec **codec,
                       enum AVCodecID codec_id)
{
    AVCodecContext *c;
    int i;

    /* find the encoder */
    *codec = avcodec_find_encoder(codec_id);
    if (!(*codec)) {
        fprintf(stderr, "Could not find encoder for '%s'\n",
                avcodec_get_name(codec_id));
        exit(1);
    }

    ost->st = avformat_new_stream(oc, *codec);
    if (!ost->st) {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }
    ost->st->id = oc->nb_streams-1;
    c = ost->st->codec;

    switch ((*codec)->type) {
    case AVMEDIA_TYPE_AUDIO:
        c->sample_fmt  = (*codec)->sample_fmts ?
            (*codec)->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
        c->bit_rate    = 64000;
        c->sample_rate = 44100;
        if ((*codec)->supported_samplerates) {
            c->sample_rate = (*codec)->supported_samplerates[0];
            for (i = 0; (*codec)->supported_samplerates[i]; i++) {
                if ((*codec)->supported_samplerates[i] == 44100)
                    c->sample_rate = 44100;
            }
        }
        c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
        c->channel_layout = AV_CH_LAYOUT_STEREO;
        if ((*codec)->channel_layouts) {
            c->channel_layout = (*codec)->channel_layouts[0];
            for (i = 0; (*codec)->channel_layouts[i]; i++) {
                if ((*codec)->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                    c->channel_layout = AV_CH_LAYOUT_STEREO;
            }
        }
        c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
        ost->st->time_base = (AVRational){ 1, c->sample_rate };
        break;

    case AVMEDIA_TYPE_VIDEO:
        c->codec_id = codec_id;

        c->bit_rate = 400000;
        /* Resolution must be a multiple of two. */
        c->width    = 640;
        c->height   = 480;
        /* timebase: This is the fundamental unit of time (in seconds) in terms
         * of which frame timestamps are represented. For fixed-fps content,
         * timebase should be 1/framerate and timestamp increments should be
         * identical to 1. */
        ost->st->time_base = (AVRational){ 1, STREAM_FRAME_RATE };
        c->time_base       = ost->st->time_base;

        c->gop_size      = 12; /* emit one intra frame every twelve frames at most */
        c->pix_fmt       = STREAM_PIX_FMT;
        if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
            /* just for testing, we also add B frames */
            c->max_b_frames = 2;
        }
        if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
            /* Needed to avoid using macroblocks in which some coeffs overflow.
             * This does not happen with normal video, it just happens here as
             * the motion of the chroma plane does not match the luma plane. */
            c->mb_decision = 2;
        }
    break;

    default:
        break;
    }

    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
}



/* open Video Stream */
static AVFrame *alloc_picture(enum AVPixelFormat pix_fmt, int width, int height)
{
    AVFrame *picture;
    int ret;

    picture = av_frame_alloc();
    if (!picture)
        return NULL;

    picture->format = pix_fmt;
    picture->width  = width;
    picture->height = height;

    /* allocate the buffers for the frame data */
    ret = av_frame_get_buffer(picture, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate frame data.\n");
        exit(1);
    }

    return picture;
}


static void open_video(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg)
{
    int ret;
    AVCodecContext *c = ost->st->codec;
    AVDictionary *opt = NULL;

    av_dict_copy(&opt, opt_arg, 0);

    /* open the codec */
    ret = avcodec_open2(c, codec, &opt);
    av_dict_free(&opt);
    if (ret < 0) {
        fprintf(stderr, "Could not open video codec: %s\n", av_err2str(ret));
        exit(1);
    }

    /* allocate and init a re-usable frame */
    // TODO: might need to change width/height
    ost->frame = alloc_picture(c->pix_fmt, c->width, c->height);
    if (!ost->frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }

    /* If the output format is not YUV420P, then a temporary YUV420P
     * picture is needed too. It is then converted to the required
     * output format. */
    ost->tmp_frame = NULL;
    if (c->pix_fmt != AV_PIX_FMT_YUV420P) {
        ost->tmp_frame = alloc_picture(AV_PIX_FMT_YUV420P, c->width, c->height);
        if (!ost->tmp_frame) {
            fprintf(stderr, "Could not allocate temporary picture\n");
            exit(1);
        }
    }
}



void Mat2AvFrame(Mat **input, AVFrame *output){

	avpicture_fill(
		(AVPicture *)video_st.tmp_frame,
		(uint8_t *)(*input)->data,
		AV_PIX_FMT_GRAY8,
		(*input)->cols,
		(*input)->rows
	);


}

void CameraSec::openStream(){
	cout << "Starting \"openStream\"" << endl;

	av_register_all();

	avformat_alloc_output_context2(&oc, NULL, "rtsp", videoStreamAddress);

	if (!oc){
		cerr << "Can't create output context" << endl;
		exit(1);
	}

	fmt = oc->oformat;

	/* Add the audio and video streams using the default format codecs
     * and initialize the codecs. */
    if (fmt->video_codec != AV_CODEC_ID_NONE) {
        add_stream(&video_st, oc, &video_codec, fmt->video_codec);
        have_video = 1;
        encode_video = 1;
    }
    if (fmt->audio_codec != AV_CODEC_ID_NONE) {
        add_stream(&audio_st, oc, &audio_codec, fmt->audio_codec);
        have_audio = 1;
        encode_audio = 1;
    }

    /* Now that all the parameters are set, we can open the audio and
     * video codecs and allocate the necessary encode buffers. */
    if (have_video)
        open_video(oc, video_codec, &video_st, opt);

    //if (have_audio)
    //    open_audio(oc, audio_codec, &audio_st, opt);

    // Print output format:
    av_dump_format(oc, 0, videoStreamAddress, 1);

    // Open write stream:
    if (!(fmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&oc->pb, videoStreamAddress, AVIO_FLAG_WRITE);
        if (ret < 0) {
            cerr << "Could not open "<< videoStreamAddress << ", " << av_err2str(ret);
            exit(1);
        }
    }

    // Write Stream Header:
    avformat_write_header(oc,&opt);

	cout << "Exiting \"openStream\"" << endl;
}

// close video stream

static void close_stream(AVFormatContext *oc, OutputStream *ost)
{
    avcodec_close(ost->st->codec);
    av_frame_free(&ost->frame);
    av_frame_free(&ost->tmp_frame);
    sws_freeContext(ost->sws_ctx);
    swr_free(&ost->swr_ctx);
}

void CameraSec::closeStream(){

	cout << "Starting \"closeStream\"" << endl;

	av_write_trailer(oc);

    close_stream(oc, &video_st);

    if (!(fmt->flags & AVFMT_NOFILE))
        /* Close the output file. */
        avio_close(oc->pb);

    /* free the stream */
    avformat_free_context(oc);

	cout << "Exiting \"closeStream\"" << endl;
}


// Functions for Stream video frames:
// ----------------------------------
static void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;

    //printf("pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
    //       av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
    //       av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
    //       av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
    //       pkt->stream_index);
    cout << "Log packet" << endl;
}

static int write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt)
{
    /* rescale output packet timestamp values from codec to stream timebase */
    av_packet_rescale_ts(pkt, *time_base, st->time_base);
    pkt->stream_index = st->index;

    /* Write the compressed frame to the media file. */
    log_packet(fmt_ctx, pkt);
    return av_interleaved_write_frame(fmt_ctx, pkt);
}


/* Prepare a dummy image. */
static void fill_yuv_image(AVFrame *pict, int frame_index,
                           int width, int height)
{
    int x, y, i, ret;

    /* when we pass a frame to the encoder, it may keep a reference to it
     * internally;
     * make sure we do not overwrite it here
     */
    ret = av_frame_make_writable(pict);
    if (ret < 0)
        exit(1);

    i = frame_index;
    /* Y */
    for (y = 0; y < height; y++)
        for (x = 0; x < width; x++){
            pict->data[0][y * pict->linesize[0] + x] = x + y + i * 3;
            //pict->data[0][y * pict->linesize[0] + x] = camFrameg.data[0][y * pict->linesize[0] + x];
        }

    /* Cb and Cr */
    for (y = 0; y < height / 2; y++) {
        for (x = 0; x < width / 2; x++) {
            pict->data[1][y * pict->linesize[1] + x] = 128 + y + i * 2;
            pict->data[2][y * pict->linesize[2] + x] = 64 + x + i * 5;
        }
    }
}


static AVFrame *get_video_frame(OutputStream *ost)
{
    AVCodecContext *c = ost->st->codec;

    /* check if we want to generate more frames */
    if (av_compare_ts(ost->next_pts, ost->st->codec->time_base,
                      STREAM_DURATION, (AVRational){ 1, 1 }) >= 0)
        return NULL;

    if (c->pix_fmt != AV_PIX_FMT_YUV420P) {
        /* as we only generate a YUV420P picture, we must convert it
         * to the codec pixel format if needed */
        if (!ost->sws_ctx) {
            ost->sws_ctx = sws_getContext(c->width, c->height,
                                          AV_PIX_FMT_YUV420P,
                                          c->width, c->height,
                                          c->pix_fmt,
                                          SCALE_FLAGS, NULL, NULL, NULL);
            if (!ost->sws_ctx) {
                fprintf(stderr,
                        "Could not initialize the conversion context\n");
                exit(1);
            }
        }
        fill_yuv_image(ost->tmp_frame, ost->next_pts, c->width, c->height);
        sws_scale(ost->sws_ctx,
                  (const uint8_t * const *)ost->tmp_frame->data, ost->tmp_frame->linesize,
                  0, c->height, ost->frame->data, ost->frame->linesize);
    } else {
        fill_yuv_image(ost->frame, ost->next_pts, c->width, c->height);
    }

    ost->frame->pts = ost->next_pts++;

    return ost->frame;
}

AVFrame* Mat2AVFrame(Mat **input){
	AVFrame* output;

    //create a AVPicture frame from the opencv Mat input image
    avpicture_fill((AVPicture *)video_st.tmp_frame,
        (uint8_t *)(*input)->data,
		AV_PIX_FMT_GRAY8,
        (*input)->cols,
        (*input)->rows);


    (*input)->release();
    return output;

}

/*
 * encode one video frame and send it to the muxer
 * return 1 when encoding is finished, 0 otherwise
 */
static int write_video_frame(AVFormatContext *oc, OutputStream *ost)
{
    int ret;
    AVCodecContext *c;
    AVFrame *frame;
    int got_packet = 0;

    c = ost->st->codec;

    frame = get_video_frame(ost);


    if (oc->oformat->flags & AVFMT_RAWPICTURE) {
        /* a hack to avoid data copy with some raw video muxers */
        AVPacket pkt;
        av_init_packet(&pkt);

        if (!frame)
            return 1;

        pkt.flags        |= AV_PKT_FLAG_KEY;
        pkt.stream_index  = ost->st->index;
        pkt.data          = (uint8_t *)frame;
        pkt.size          = sizeof(AVPicture);

        pkt.pts = pkt.dts = frame->pts;
        av_packet_rescale_ts(&pkt, c->time_base, ost->st->time_base);

        ret = av_interleaved_write_frame(oc, &pkt);
    } else {
        AVPacket pkt = { 0 };
        av_init_packet(&pkt);


        /* encode the image */
        ret = avcodec_encode_video2(c, &pkt, frame, &got_packet);
        if (ret < 0) {
            fprintf(stderr, "Error encoding video frame: %s\n", av_err2str(ret));
            exit(1);
        }

        if (got_packet) {
            ret = write_frame(oc, &c->time_base, ost->st, &pkt);
        } else {
            ret = 0;
        }
    }

    if (ret < 0) {
        fprintf(stderr, "Error while writing video frame: %s\n", av_err2str(ret));
        exit(1);
    }

    return (frame || got_packet) ? 0 : 1;
}


void CameraSec::streamThread(){

	cout << "Starting \"streamThread\"" << endl;

	// Initiate Camera:
	Mat camFrame;

	VideoCapture capture(0);
	if (!capture.isOpened()){
		cerr<<"Error opening camera, Exiting"<<endl;
		exit(1);
	}
	cameraState = true;

	// Initiate Stream
	openStream();

	while (status.compare("STRM") == 0){
		cout << "Streamming" << endl;
		capture >> camFrame;
		cvtColor(camFrame,camFrameg,CV_BGR2YCrCb);

		// translate camFrame to frame

		// write video frame to stream:
		write_video_frame(oc, &video_st);
		cout << "finished writing frame" << endl;
	}


	// close stream
	closeStream();

	// release camera
	cout << "Received global status " << status << endl;
	cameraState = false;
	capture.release();


	cout << "Exiting \"streamThread\"" << endl;
}



