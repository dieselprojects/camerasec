/*
 * CameraSec.h
 *
 *  Created on: Dec 12, 2014
 *      Author: idanhahn
 */

#ifndef SRC_CAMERASEC_H_
#define SRC_CAMERASEC_H_

#include <string>
#include <cv.h>
using namespace std;
using namespace cv;



// -------------------------------- //
// --    Class Camera Security   -- //
// -------------------------------- //


class CameraSec {
public:

	// Class variables:
	// ----------------
	bool cameraState;

	CameraSec();
	virtual ~CameraSec();



	// Class functions:
	// ----------------
	void start();

	void monitor();

	void monitorThread();

	void streamThread();

	void openStream();

	void closeStream();


};

#endif /* SRC_CAMERASEC_H_ */
